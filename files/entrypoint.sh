#!/bin/bash
set -e

trap appStop SIGINT SIGTERM

# Variables
SONAR_HOST=${SONAR_HOST:-localhost}
SONAR_PORT=${SONAR_PORT:-9000}
SONAR_LOG_LEVEL=${SONAR_LOG_LEVEL:-INFO}
SONAR_VERBOSE=${SONAR_VERBOSE:-false}
SONAR_PROJECT=${SONAR_PROJECT}
SONAR_TOKEN=${SONAR_TOKEN}


# Configure sonar-scanner.properties
sed -i 's|#sonar.host.url=http://localhost:9000|sonar.host.url=http://'"${SONAR_HOST}"':'"${SONAR_PORT}"'|g' /sonar-scanner/conf/sonar-scanner.properties
sed -i 's|#sonar|sonar|g' /sonar-scanner/conf/sonar-scanner.properties

appStart () {
  echo "Starting sonar-scanner..."
  set +e
  sonar-scanner  -D"sonar.log.level=${SONAR_LOG_LEVEL}" -D"sonar.verbose=${SONAR_VERBOSE}" -D"sonar.projectKey=${SONAR_PROJECT}" -D"sonar.login=${SONAR_TOKEN}"
}

appStop () {
  echo "Stopping sonar-scanner..."
  set +e
}

appHelp () {
  sonar-scanner -h
}

case "$1" in
  app:start)
    appStart
    ;;
  app:stop)
    appStop
    ;;
  app:help)
    appHelp
    ;;
  *)
    if [ -x $1 ]; then
      $1
    else
      prog=$(which $1)
      if [ -n "${prog}" ] ; then
        shift 1
        $prog $@
      else
        appHelp
      fi
    fi
    ;;
esac

exit 0
