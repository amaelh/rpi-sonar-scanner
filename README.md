# Sonar Scanner

This Docker container implements a Sonar Scanner for raspberry pi, based on a balenalib base image.

### Installation

You can download the image with the following command:

```bash
docker pull amaelh/rpi-sonar-scanner
```

# How to use this image

Exposed volume
----

The image exports one volume: `/var/scanner`, used to store all the codes to be analyzed.

Use cases

Environment variables
----

This image uses environment variables to allow the configuration of some parameteres at run time. See example in docker-compose below :

```
version: "3.4"

services:

  myProjectScanner:
    image: amaelh/rpi-sonar-scanner:latest
    environment:
      - SONAR_HOST=192.168.0.40
      - SONAR_PORT=9000
      - SONAR_LOG_LEVEL=INFO
      - SONAR_VERBOSE=false
      - SONAR_PROJECT=myProject
      - SONAR_TOKEN=1212345678902345678900123456789012345678
    volumes:
      - ~/workspace/myProject:/var/scanner:rw
      - /etc/localtime:/etc/localtime:ro

```
