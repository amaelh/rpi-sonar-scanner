FROM balenalib/raspberrypi3-openjdk:11-jre-run
MAINTAINER Amael H. <amael@heiligtag.com>

ENV SONAR_SCANNER_VERSION 4.3.0.2102
ENV SONAR_SCANNER_OPTS -Xmx512m 
ENV PATH $PATH:/sonar-scanner/bin 

RUN apt-get update \
    && apt-get install -y \
    wget \
    unzip \
    nodejs \
    node-typescript \
    && cd / \
    && wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-${SONAR_SCANNER_VERSION}-linux.zip \
    && unzip sonar-scanner-cli-${SONAR_SCANNER_VERSION}-linux.zip \
    && rm -f sonar-scanner-cli-${SONAR_SCANNER_VERSION}-linux.zip \
    && mv /sonar-scanner-${SONAR_SCANNER_VERSION}-linux /sonar-scanner \
    && sed -i "s/use_embedded_jre=true/use_embedded_jre=false/" /sonar-scanner/bin/sonar-scanner \
    && mkdir /var/scanner \
    && apt-get purge --auto-remove wget unzip \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /var/scanner
VOLUME /var/scanner

COPY files/entrypoint.sh /entrypoint.sh
RUN chmod 755 /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

CMD ["app:start"]
